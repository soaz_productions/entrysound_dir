package main

import (
	"bytes"
	"context"
	"encoding/json"
	"entrysound_dir/transcoder"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"

	"cloud.google.com/go/storage"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

var (
	keyFile        = flag.String("key", "/etc/key.json", "Path to JSON Key File to authenticate with Firebase")
	baseURL        = flag.String("base-url", "http://localhost", "Redirect URL for discord OAuth")
	clientID       = flag.String("clientID", "", "Client ID for discord OAuth")
	firebaseBucket = flag.String("bucket", "discord-entrysound.appspot.com", "Firebase Bucket Name")
	redisURL       = flag.String("redis", "redis:6379", "REDIS URL")
)

type botNotifier interface {
	Notify() error
}

var notify botNotifier

func main() {
	flag.Parse()
	s := InitializeServer()
	notify = ProvideNotifier()
	r := mux.NewRouter()
	cors1 := handlers.AllowedOrigins([]string{"*"})
	cors2 := handlers.AllowedHeaders([]string{"Authorization", "X-Requested-With", "Content-Type"})
	cors3 := handlers.AllowedMethods([]string{http.MethodPost, http.MethodDelete, http.MethodGet, http.MethodOptions})
	r.HandleFunc("/mp3", s.HandleUploadMP3).Methods("POST")
	r.HandleFunc("/mp3", s.HandleDeleteMP3).Methods("DELETE")
	r.HandleFunc("/conf", s.HandleGetConf)
	r.HandleFunc("/sounds/my", s.HandleGetMySound)
	r.HandleFunc("/sounds/{id}", s.HandleGetSound)
	r.HandleFunc("/sounds/{id}/lastUpdated", s.HandleGetSoundInfo)
	http.ListenAndServe(":80", handlers.CORS(cors1, cors2, cors3, handlers.AllowCredentials())(r))
}

type server struct {
	configGetter UserConfigGetter
	logger       logrus.FieldLogger
	bucket       *storage.BucketHandle
}

func (s *server) HandleDeleteMP3(w http.ResponseWriter, r *http.Request) {
	accessToken := r.Header.Get("Authorization")
	discordUser, err := s.getUsername(accessToken)
	if err != nil {
		s.handleError(w, err)
		return
	}
	err = s.bucket.Object(fmt.Sprintf("%s.mp3", discordUser)).Delete(r.Context())
	if err != nil {
		s.handleError(w, err)
		return
	}
	err = s.bucket.Object(fmt.Sprintf("%s.dca", discordUser)).Delete(r.Context())
	if err != nil {
		s.handleError(w, err)
		return
	}
	err = notify.Notify()
	if err != nil {
		s.handleError(w, err)
		return
	}
	s.logger.WithField("user_id", discordUser).Info("Deleted DCA from Storage")
	w.WriteHeader(200)
	return
}

func (s *server) HandleUploadMP3(w http.ResponseWriter, r *http.Request) {
	accessToken := r.Header.Get("Authorization")
	discordUser, err := s.getUsername(accessToken)
	if err != nil {
		s.handleError(w, err)
		return
	}
	bodyReader := io.LimitReader(r.Body, 102400)
	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		s.handleError(w, err)
		return
	}
	defer r.Body.Close()
	var buf = &bytes.Buffer{}
	err = transcoder.TranscodeMP3ToDCA(body, buf)
	if err != nil {
		s.handleError(w, err)
		return
	}
	writer := s.bucket.Object(fmt.Sprintf("%s.dca", discordUser)).NewWriter(r.Context())
	_, err = io.Copy(writer, buf)
	if err != nil {
		s.handleError(w, err)
		return
	}
	err = writer.Close()
	if err != nil {
		s.handleError(w, err)
		return
	}
	writer = s.bucket.Object(fmt.Sprintf("%s.mp3", discordUser)).NewWriter(r.Context())
	_, err = writer.Write(body)
	if err != nil {
		s.handleError(w, err)
		return
	}
	err = writer.Close()
	if err != nil {
		s.handleError(w, err)
		return
	}
	err = notify.Notify()
	if err != nil {
		s.handleError(w, err)
		return
	}
	s.logger.WithField("user_id", discordUser).Info("Wrote DCA to Storage")
	w.WriteHeader(200)
	return
}

func (s *server) HandleGetConf(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	c, err := s.configGetter.GetUserConfig(r.Context())
	if err != nil {
		s.handleError(w, err)
		return
	}
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(c)
}

func (s *server) HandleGetSound(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]
	filename := fmt.Sprintf("%s.dca", userID)
	param, ok := r.URL.Query()["type"]
	if ok {
		filename = fmt.Sprintf("%s.%s", userID, param[0])
	}
	reader, err := s.bucket.Object(filename).NewReader(r.Context())
	if err == storage.ErrObjectNotExist {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		s.handleError(w, err)
		return
	}
	io.Copy(w, reader)
}

type lastUpdated struct {
	UserID      string    `json:"user_id"`
	LastUpdated time.Time `json:"last_updated"`
}

func (s *server) HandleGetSoundInfo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]
	objAttrs, err := s.bucket.Object(fmt.Sprintf("%s.dca", userID)).Attrs(context.Background())
	if err == storage.ErrObjectNotExist {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		s.handleError(w, err)
		return
	}
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(lastUpdated{UserID: userID, LastUpdated: objAttrs.Updated})
}
func (s *server) HandleGetMySound(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	accessToken := r.Header.Get("Authorization")
	discordUser, err := s.getUsername(accessToken)
	if err != nil {
		w.WriteHeader(403)
		s.logger.Warn("Unauthorized Access (probably)")
		return
	}
	filename := fmt.Sprintf("%s.mp3", discordUser)
	reader, err := s.bucket.Object(filename).NewReader(ctx)
	if err == storage.ErrObjectNotExist {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		s.handleError(w, err)
		return
	}
	w.Header().Set("Content-Type", "audio/mpeg")
	w.WriteHeader(200)
	io.Copy(w, reader)
}

func (s *server) handleError(w http.ResponseWriter, err error) {
	w.WriteHeader(500)
	s.logger.WithError(err).Error("Error Handling Request")
	return
}

func (s *server) getUsername(accessToken string) (string, error) {
	discord, err := discordgo.New("Bearer " + accessToken)
	if err != nil {
		return "", errors.Wrap(err, "could not open discordgo client")
	}
	user, err := discord.User("@me")
	if err != nil {
		return "", errors.Wrap(err, "could not retrieve User @me")
	}
	return user.ID, nil
}

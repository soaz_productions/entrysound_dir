FROM monomo/golang-ffmpeg:stable
WORKDIR /entrysound_dir
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build
CMD ["./entrysound_dir"]
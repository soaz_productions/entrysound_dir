package notifier

import (
	"time"

	"github.com/pkg/errors"

	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
)

func New(l logrus.FieldLogger, pool *redis.Pool) *Notifier {
	return &Notifier{
		logger: l.WithField("comp", "notifier"),
		pool:   pool,
	}
}

type Notifier struct {
	logger logrus.FieldLogger
	pool   *redis.Pool
}

func (n *Notifier) Notify() error {
	conn := n.pool.Get()
	defer conn.Close()
	_, err := conn.Do("PUBLISH", "update", time.Now().UTC().Format(time.RFC3339))
	if err != nil {
		n.logger.WithError(err).Error("Could not Publish Update")
		return errors.Wrap(err, "could not notify bots of update")
	}
	return nil
}

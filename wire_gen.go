// Code generated by Wire. DO NOT EDIT.

//go:generate wire
//+build !wireinject

package main

import (
	"cloud.google.com/go/storage"
	"context"
	"entrysound_dir/notifier"
	"firebase.google.com/go"
	"github.com/gomodule/redigo/redis"
	"github.com/google/wire"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/api/option"
	"os"
	"time"
)

// Injectors from wire.go:

func InitializeServer() *server {
	fieldLogger := NewLogger()
	mainBucketNameGetter := ProvideBucketNameGetter()
	bucketHandle := ProvideBucket(fieldLogger, mainBucketNameGetter)
	mainServerURLGetter := ProvideServerURLGetter()
	firebaseConfigGetter := NewConfigGetter(fieldLogger, bucketHandle, mainServerURLGetter)
	mainServer := NewServer(firebaseConfigGetter, fieldLogger, bucketHandle)
	return mainServer
}

func ProvideNotifier() *notifier.Notifier {
	fieldLogger := NewLogger()
	mainRedisURLGetter := ProvideRedisURLGetter()
	pool := NewRedis(mainRedisURLGetter, fieldLogger)
	notifierNotifier := notifier.New(fieldLogger, pool)
	return notifierNotifier
}

// wire.go:

var loggerSet = wire.NewSet(NewLogger)

var confGetterSet = wire.NewSet(
	NewConfigGetter, wire.Bind((*UserConfigGetter)(nil), (*FirebaseConfigGetter)(nil)), ProvideBucket,
	ProvideServerURLGetter,
	ProvideBucketNameGetter,
	loggerSet)

func NewServer(c UserConfigGetter, l logrus.FieldLogger, b *storage.BucketHandle) *server {
	return &server{
		configGetter: c,
		logger:       l,
		bucket:       b,
	}
}

func NewLogger() logrus.FieldLogger {
	l := logrus.New()
	l.SetOutput(os.Stdout)
	l.SetFormatter(&logrus.JSONFormatter{})
	return l.WithField("app", "entrysound_dir")
}

func NewConfigGetter(logger logrus.FieldLogger, bucket *storage.BucketHandle, baseURL2 serverURLGetter) *FirebaseConfigGetter {
	return &FirebaseConfigGetter{logger: logger, bucket: bucket, BaseURL: baseURL2()}
}

func ProvideBucket(logger logrus.FieldLogger, name bucketNameGetter) *storage.BucketHandle {
	config := &firebase.Config{
		StorageBucket: name(),
	}
	opt := option.WithCredentialsFile(*keyFile)
	app, err := firebase.NewApp(context.Background(), config, opt)
	if err != nil {
		panic(err)
	}
	client, err := app.Storage(context.Background())
	if err != nil {
		panic(err)
	}
	bucket, err := client.DefaultBucket()
	if err != nil {
		panic(err)
	}
	return bucket
}

type bucketNameGetter func() string

func ProvideBucketNameGetter() bucketNameGetter {
	return bucketNameGetter(func() string {
		return *firebaseBucket
	})
}

type serverURLGetter func() ServerURL

func ProvideServerURLGetter() serverURLGetter {
	return serverURLGetter(func() ServerURL {
		return ServerURL(*baseURL)
	})
}

type redisURLGetter func() string

func ProvideRedisURLGetter() redisURLGetter {
	return redisURLGetter(func() string {
		return *redisURL
	})
}

func NewRedis(redisURL2 redisURLGetter, logger logrus.FieldLogger) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", redisURL2())
			if err != nil {
				logger.WithField("comp", "redis").WithError(err).Error("Could not connect to redis")
				return nil, errors.Wrapf(err, "could not dial %s", redisURL2())
			}
			return c, nil
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}
